// Assignment 4

// TASK1
const task1Form = document.getElementById("task1Form");
const task1Input = document.getElementById("task1Input");
const task1Output = document.getElementById("task1Output");

task1Form.addEventListener("submit", charReverseHandler);

function charReverseHandler(e) {
  e.preventDefault();

  let newArrWords = "";

  const words = task1Input.value;
  const arrWords = words.split(" ");
  arrWords.forEach((word) => {
    newArrWords += ` ${
      word.charAt().toLowerCase() + word.slice(1).toUpperCase()
    }`;
  });

  console.log(newArrWords.trimStart());
  task1Output.textContent = newArrWords;
}

// TASK2
const task2Form = document.getElementById("task2Form");
const task2Input = document.getElementById("task2Input");
const task_listEl = document.getElementById("task_list");
task2Form.addEventListener("submit", createListHandler);

let task_list = [];

function createListHandler(e) {
  e.preventDefault();

  const task = task2Input.value;
  if (task) {
    task_list.push(task);

    const div = document.createElement('div')
    div.classList.add("task-container")
    const li = document.createElement("li");
    li.classList.add("list-item");
    li.textContent = task_list[task_list.length - 1];3

    div.appendChild(li)

    const button = document.createElement('button')
    button.classList.add("trash-button")
    button.innerHTML = `<i class="fas fa-trash"></i>`

    li.appendChild(button)

    task_listEl.appendChild(div);
    // console.log(task_list[task_list.length-1]);

    // CLEAR INPUT
    task2Input.value = "";
  }
}

document.addEventListener("click", function (e) {
    console.log(e.target.parentElement);
  if (e.target.classList.value == "trash-button") {
    const item = e.target.parentElement;
    item.remove();
  }
});

// TAKS3
let arr = [1, 2, 2, 3, 4, 5, 6, 5, 6];
let sortArr = [];
let duplicateValues = [];

function findDublicatedHandler(array) {
  array.forEach((item) => {
    if (!sortArr.includes(item)) {
      sortArr.push(item);
    } else {
      duplicateValues.push(item);
    }
  });
}

// findDublicatedHandler(arr);
// console.log("This is duplicate values", duplicateValues);

// TASK4
const unionHandler = (arr1, arr2) => Array.from(new Set(arr1.concat(arr2)));
// console.log(unionHandler([1, 2, 3, 4, 5], [1, "a", "f", 3, 4, 5]));

// TASK5
const arr5 = [1, 2, 3, null, 0, "", false, undefined, NaN];
const answer = [];
function onlyTruthy(arr) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i]) {
      answer.push(arr[i]);
    }
  }
  console.log(answer);
}

// onlyTruthy(arr5);

// TASK6
function reverseStringHandler(word) {
  const sortedWords = word
    .split("")
    .sort((a, b) => a.localeCompare(b))
    .join("");
  console.log(sortedWords);
}

// reverseStringHandler("AlWLAAch");


